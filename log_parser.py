import numpy as np
import argparse
import os.path
import json

from helicopter import Helicopter

dirname = os.path.dirname(__file__)
filename = os.path.join(dirname, 'archer.json')

parser = argparse.ArgumentParser()
parser.add_argument('input', help='Input .log file')
parser.add_argument('--output', '-o', dest='output', help='Output .log file, append _PTCH to input by default')
parser.add_argument('--config', '-c', dest='config', help='Config .json file, default archer.json', default=filename)
args = parser.parse_args()

heli = Helicopter(json.load(open(args.config, 'r')))
print('Created Helicopter with config ' + args.config)

available_fmt_ids = np.ones(256)

print('Opening ' + args.input)

if args.output is None:
    assert args.input[-4:].lower() == '.log'
    args.output = args.input[:-4] + '_PTCH' + args.input[-4:]

with open(args.input, 'r') as infile:
    with open(args.output, 'w') as outfile:
        line = infile.readline()
        outfile.write(line)
        while line.startswith('FMT'):
            words = line.split(', ')
            fmt_id = int(words[1])
            available_fmt_ids[fmt_id] = 0

            line = infile.readline()
            outfile.write(line)

        ptch_id = np.nonzero(available_fmt_ids)[0][0]
        # print('Available ids:')
        # print(np.nonzero(available_fmt_ids))
        print(f'Message PTCH assigned id {ptch_id}')
        outfile.write(f'FMT, {ptch_id}, 23, PTCH, Qfff, TimeUS,Coll,CycPitch,CycRoll\n')
        outfile.write(f'FMTU, 1, {ptch_id}, sddd, F111\n')

        for inline in infile:  # continues iteration where last readline finished
            outfile.write(inline)
            if inline.startswith('RCOU'):
                words = inline.split(', ')
                time_us = words[1]
                pwm = np.array([
                    int(words[2]),
                    int(words[4]),
                    int(words[3])
                ])  # aileron, elevator, collective is motor 1, 3, 2
                pitch = heli.calc_pitch(pwm)
                outfile.write(f'PTCH, {time_us}, {pitch[0]}, {pitch[1]}, {pitch[2]}\n')
