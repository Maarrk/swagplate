import unittest
import numpy as np

from rotorhub import Rotorhub
import dictionary_keys as dk


class RotorhubTestCase(unittest.TestCase):
    def test_solve_quad(self):
        rh = Rotorhub()
        
        # paralellogram setup
        rods = 4.0
        levers = 1.0
        rh.load_config({
            dk.rotorhub: {
                dk.rod_length: rods,
                dk.lever_length: levers
            },
            dk.swashplate: {
                dk.lever_length: levers
            }
        })

        angle = np.deg2rad(15)
        quad_params = np.array([[np.pi / 2 - angle, rods]])
        self.assertTrue(np.allclose(rh.solve_quad(quad_params), angle), 'parallelogram')

        # right triangle
        rh.load_config({
            dk.rotorhub: {
                dk.rod_length: 5.0,
                dk.lever_length: np.sqrt(2)
            },
            dk.swashplate: {
                dk.lever_length: 4.0
            }
        })
        quad_params = np.array([[np.pi / 2, 3.0]])
        self.assertTrue(np.allclose(rh.solve_quad(quad_params), np.deg2rad(45)), 'right triangle')

    def test_calc_pitch(self):
        rh = Rotorhub()

        # paralellogram setup
        rods = 4.0
        levers = 1.0
        rh.load_config({
            dk.rotorhub: {
                dk.rod_length: rods,
                dk.lever_length: levers
            },
            dk.swashplate: {
                dk.lever_length: levers
            }
        })

        sp_roll = np.deg2rad(21)
        sp_pitch = np.deg2rad(37)
        sp_state = np.array([sp_roll, sp_pitch, rods])

        self.assertTrue(np.allclose(rh.calc_pitch(sp_state), np.array([0, sp_pitch, sp_roll])), 'cyclic values')

        sp_state = np.array([sp_roll, sp_pitch, rods - levers / 2])
        self.assertTrue(rh.calc_pitch(sp_state)[0] > levers / 4, 'collective positive direction')


if __name__ == '__main__':
    unittest.main()
