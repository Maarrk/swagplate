import numpy as np
import json

import dictionary_keys as dk
from servo import Servo
from swashplate import Swashplate
from rod_solver import RodSolver
from rotorhub import Rotorhub


def main():
    servos = [None, None, None]
    rod_lengths = np.zeros(3, dtype=np.float_)
    config = json.load(open('archer.json', 'r'))

    for servo_config in config[dk.servos]:
        srv = Servo()
        srv.load_config(servo_config)
        servos[dk.channel_order[servo_config[dk.channel]]] = srv
        rod_lengths[dk.channel_order[servo_config[dk.channel]]] = servo_config[dk.rod_length]

    swashplate = Swashplate()
    swashplate.load_config(config[dk.swashplate])

    rodsolver = RodSolver(config, swashplate, rod_lengths)

    rotorhub = Rotorhub()
    rotorhub.load_config(config)

    # order needs to be aileron, elevator, collective so motor 1, 3, 2 as seen by ardupilot in H3_120 rotor servos
    pwm_trim = np.array([
        1475,
        1520,
        1350
    ])

    pwm_max = 1673
    pwm_mid = 1500
    pwm_min = 1465

    # pwm = pwm_trim - (pwm_max - pwm_mid)
    pwm = pwm_trim
    # pwm = pwm_trim - (pwm_min - pwm_mid)
    print(pwm)

    servo_snaps = np.concatenate((
        servos[0].snap_pos(pwm[0]),
        servos[1].snap_pos(pwm[1]),
        servos[2].snap_pos(pwm[2]),
    ), axis=1)
    sp_state = rodsolver.solve_for_sp_state(servo_snaps)

    print('roll: {:.2f}deg, pitch:{:.2f}deg, slide:{:.2f}mm'.format(
        np.rad2deg(sp_state[0]), np.rad2deg(sp_state[1]), sp_state[2]))

    pitch = rotorhub.calc_pitch(sp_state)

    print('pitch [deg]:')
    print(np.rad2deg(pitch))


if __name__ == '__main__':
    main()
