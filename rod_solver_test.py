import unittest
import numpy as np

from rod_solver import RodSolver
from servo import Servo
from swashplate import Swashplate
import dictionary_keys as dk


class RodSolverTestCase(unittest.TestCase):
    def test_solve_linear(self):
        config = {
            dk.swashplate: {
                dk.position: {
                    dk.z: 0.0
                }
            },
            dk.solver: {
                dk.max_iterations: 10,
                dk.tolerance: 0.01,
                dk.verbose: False
            }
        }
        swashplate = Swashplate()
        solver = RodSolver(config, swashplate, np.array([1, 1, 1]))
        servo_snaps = np.transpose(np.array([
            [1, -1, 0.5, 1],
            [-1, 0, 0.5, 1],
            [1, 1, 0.5, 1]
        ]))
        self.assertTrue(np.allclose(solver.solve_for_sp_state(servo_snaps), np.array([0, 0, -0.5])), "translate up")


if __name__ == '__main__':
    unittest.main()
