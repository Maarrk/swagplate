"""
Run this script to format all json data files in this folder.
Always run before committing
"""

import os
import json

json_files = [f for f in os.listdir('.') if '.json' in f]

for filename in json_files:
    with open(filename) as data_file:
        data_loaded = json.load(data_file)

    with open(filename, 'w') as out_file:
        json.dump(data_loaded, out_file, sort_keys=True, indent=2)
