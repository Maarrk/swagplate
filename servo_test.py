import unittest
import numpy as np

from servo import Servo


class ServoTestCase(unittest.TestCase):
    def test_lever_angle(self):
        srv = Servo()
        self.assertEqual(srv._lever_angle(1100.0), -np.radians(45.0), 'low')
        self.assertEqual(srv._lever_angle(1500.0), 0.0, 'mid')
        self.assertEqual(srv._lever_angle(1900.0), np.radians(45.0), 'high')
        self.assertTrue(srv._lever_angle(1600.0) > 0.0, 'mid-high')

    def test_snap_pos(self):
        srv = Servo()
        self.assertTrue(np.all(srv.transform == np.identity(4)), 'ensure default transform')
        self.assertTrue(np.allclose(srv.snap_pos(1500.0), np.array([[0], [-1], [0], [1]])), 'centered lever')
        srv.lever_length = np.sqrt(2)
        self.assertTrue(np.allclose(srv.snap_pos(1900.0), np.array([[0], [-1], [-1], [1]])), 'high lever')
        srv.lever_length = 1.0
        srv.transform[2, 3] = 4.0  # Translate by [0 0 4]
        self.assertTrue(np.allclose(srv.snap_pos(1500.0), np.array([[0], [-1], [4], [1]])), 'servo moved lower')

    def test_str(self):
        srv = Servo()
        self.assertEqual(str(srv), 'Servo pos[0.0 0.0 0.0]', 'default string summary')
        srv.transform[0, 3] = 0.99999999999
        srv.transform[1, 3] = -0.0000001
        srv.transform[2, 3] = -12345.0
        self.assertEqual(str(srv), 'Servo pos[1.0 -0.0 -12345.0]', 'rounding in string summary')


if __name__ == '__main__':
    unittest.main()
