import unittest
import numpy as np

from swashplate import Swashplate
import dictionary_keys as dk


class SwashplateTestCase(unittest.TestCase):
    def test_local_snaps_pos(self):
        sp = Swashplate()
        sp.load_config({
            dk.width: 3.0,
            dk.front: 5.0,
            dk.back: 7.0
        })
        self.assertTrue(np.allclose(sp.local_snaps[:2, :], np.array([[5.0, -7.0, 5.0],
                                                                     [-1.5, 0, 1.5]],
                                                                    dtype=np.float_)), 'config snaps')

    def test_snaps_pos(self):
        sp = Swashplate()
        self.assertTrue(np.allclose(sp.snaps_pos(np.zeros(3)), np.array([[1, -1, 1],
                                                                         [-1, 0, 1],
                                                                         [0, 0, 0],
                                                                         [1, 1, 1]])), 'default state')
        sp.local_snaps[1, 0] = -2.0
        angle = np.radians(60.0)
        self.assertTrue(np.allclose(sp.snaps_pos(np.array([angle, 0, 0]))[1, 0], -1.0), 'only roll')

        sp.local_snaps[1, 0] = -7.0
        self.assertTrue(np.allclose(sp.snaps_pos(np.array([0, 0, 5.0])), np.array([[1, -1, 1],
                                                                                   [-7, 0, 1],
                                                                                   [5, 5, 5],
                                                                                   [1, 1, 1]])), 'only slide')

        self.assertTrue(np.allclose(sp.snaps_pos(np.array([3, 5, 7]))[1, 1], 0.0), 'back stays in symmetry')

    def test_dof_differentials(self):
        sp = Swashplate()
        tangents = sp.dof_differentials()
        self.assertTrue(np.allclose(tangents[0], np.array([[0, 0, -1],
                                                           [0, 0, 0],
                                                           [0, 0, 1]])), 'default roll')
        self.assertTrue(np.allclose(tangents[1], np.array([[0, 0, -1],
                                                           [0, 0, 1],
                                                           [0, 0, -1]])), 'default pitch')
        self.assertTrue(np.allclose(tangents[2], np.array([[0, 0, 1],
                                                           [0, 0, 1],
                                                           [0, 0, 1]])), 'default slide')
        angle = np.radians(45.0)
        hr2 = np.sqrt(2) / 2  # HalfRoot2
        tangents = sp.dof_differentials(np.array([angle, 0, 5.0]))
        self.assertTrue(np.allclose(tangents[0], np.array([[0, hr2, -hr2],
                                                           [0, 0, 0],
                                                           [0, -hr2, hr2]])), 'diagonal roll: roll')
        self.assertTrue(np.allclose(tangents[1], np.array([[-hr2, 0, -1],
                                                           [0, 0, 1],
                                                           [hr2, 0, -1]])), 'diagonal roll: pitch')
        self.assertTrue(np.allclose(tangents[2], np.array([[0, 0, 1],
                                                           [0, 0, 1],
                                                           [0, 0, 1]])), 'diagonal roll: slide')

        tangents = sp.dof_differentials(np.array([0, angle, 3.0]))
        self.assertTrue(np.allclose(tangents[0], np.array([[-hr2, 0, -hr2],
                                                           [0, 0, 0],
                                                           [hr2, 0, hr2]])), 'diagonal pitch: roll')
        self.assertTrue(np.allclose(tangents[1], np.array([[-hr2, 0, -hr2],
                                                           [hr2, 0, hr2],
                                                           [-hr2, 0, -hr2]])), 'diagonal pitch: pitch')
        self.assertTrue(np.allclose(tangents[2], np.array([[0, 0, 1],
                                                           [0, 0, 1],
                                                           [0, 0, 1]])), 'diagonal pitch: slide')


if __name__ == '__main__':
    unittest.main()
