# swagplate

Tool for calculating helicopter blade pitch from servomotor input. The geometry is based on ALIGN T-Rex 700 flybarless acro helicopter.

## Dependencies

Python 3.x with numpy package

-----

Developed by Marek Łukasiewicz for A.R.C.H.E.R. project at Warsaw Univeristy of Technology

![ARCHER logo](archer_logo.png)
